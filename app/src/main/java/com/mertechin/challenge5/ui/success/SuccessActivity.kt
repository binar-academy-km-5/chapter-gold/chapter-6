package com.mertechin.challenge5.ui.success

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivitySuccessBinding

class SuccessActivity : AppCompatActivity() {
	private lateinit var binding: ActivitySuccessBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		binding = ActivitySuccessBinding.inflate(layoutInflater)
		setContentView(binding.root)

		binding.btnHome.setOnClickListener{
			// Lanjut ke Halaman Berikutnya
			val intent = Intent(this, MainActivity::class.java)
			startActivity(intent)
		}
	}
}