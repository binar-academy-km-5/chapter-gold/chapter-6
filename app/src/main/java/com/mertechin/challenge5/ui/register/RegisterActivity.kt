package com.mertechin.challenge5.ui.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivityRegisterBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterActivity : AppCompatActivity() {
//	private lateinit var vmRegister: RegisterViewModel
	private lateinit var binding : ActivityRegisterBinding
	private val vmRegister: RegisterViewModel by viewModel()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityRegisterBinding.inflate(layoutInflater)
		setContentView(binding.root)

//		vmRegister = ViewModelProvider(this).get(RegisterViewModel::class.java)
		vmRegister.initViewModel(this)

		binding.btnRegister.setOnClickListener {
			// Ambil nilai dari form Login
			val user    = binding.etUsername.text.toString()
			val email   = binding.etEmail.text.toString()
			val pass    = binding.etPassword.text.toString()
			val notelp  = binding.etNoTelp.text.toString()

			// Firebase Register Algorithm
			vmRegister.registerFirebase(email,pass,object : RegisterViewModel.RegisterCallback{
				override fun onRegisterSuccess() {
					// Masukkan Data User ke dalam Database
					vmRegister.registerDatabase(user, pass, email, notelp)

					// Munculin Pop-up
					val toast = Toast.makeText(this@RegisterActivity,"Registrasi berhasil! Selamat datang, "+user+"!", Toast.LENGTH_SHORT)
					toast.show()

					// pindah ke activity selanjutnya
					val intent = Intent(this@RegisterActivity,MainActivity::class.java)
					startActivity(intent)
				}

				override fun onRegisterFailure() {
					val toast = Toast.makeText(this@RegisterActivity,"Registrasi gagal!", Toast.LENGTH_SHORT)
					toast.show()
				}

			})

			// Move to Next Page..
			val i = Intent(this,MainActivity::class.java)
			startActivity(i)
		}
	}
}