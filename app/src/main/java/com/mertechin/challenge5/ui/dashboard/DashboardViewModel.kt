package com.mertechin.challenge5.ui.dashboard

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mertechin.challenge5.api.ApiClient
import com.mertechin.challenge5.api.ApiServices
import com.mertechin.challenge5.dataclass.api_dataclass.ListData
import com.mertechin.challenge5.dataclass.api_dataclass.ListMenu
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardViewModel : ViewModel() {
	private val apiServices: ApiServices = ApiClient.ApiClient.instance
	private lateinit var sharedPreferences: SharedPreferences
	private val NILAI_GRID = "IS_GRID"
	private val _menu = MutableLiveData<List<ListData>>()
	val menu : LiveData<List<ListData>> = _menu

	fun initSharedPref(context: Context){
		sharedPreferences = context.getSharedPreferences("spGrid", Context.MODE_PRIVATE)
	}

	fun getSharedPref(value_grid : Boolean) : Boolean{
		return sharedPreferences.getBoolean(NILAI_GRID,value_grid)
	}

	fun changeValue(value: Boolean){
		val editor = sharedPreferences.edit()
		editor.putBoolean(NILAI_GRID, value)
		editor.apply()
	}

	fun getMenuData(){
		val call = apiServices.getListMenu()
		call.enqueue(object : Callback<ListMenu> {
			override fun onResponse(call: Call<ListMenu>, response: Response<ListMenu>) {
				if (response.isSuccessful){
					val responseBody    = response.body()
					if(responseBody != null){
						_menu.value     = responseBody.data
//						Log.e("check Fetching", response.body().toString())
					}
				}
			}

			override fun onFailure(call: Call<ListMenu>, t: Throwable) {
				Log.e("check Fetching", t.toString())
			}
		})
	}
}