package com.mertechin.challenge5.adapter

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mertechin.challenge5.R
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran
import com.mertechin.challenge5.ui.cart.CartViewModel


class CartAdapter(private val data: List<Restoran>, private val vmCart: CartViewModel) : RecyclerView.Adapter<MenuHolder>() {
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {
		val inflater = LayoutInflater.from(parent.context)
		val view = inflater.inflate(R.layout.cart_menu_item, parent, false)
		return MenuHolder(view)
	}

	override fun onBindViewHolder(holder: MenuHolder, position: Int) {
		val menuHolder = holder
		menuHolder.onBind(data[position], vmCart)
	}

	override fun getItemCount(): Int = data.size
}

class MenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
	val ivGambar: ImageView = itemView.findViewById(R.id.ivCartProfile)
	val tvNama: TextView = itemView.findViewById(R.id.tvJudul)
	val tvHarga: TextView = itemView.findViewById(R.id.tvHarga)
	val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)
	val ivMinus: ImageView = itemView.findViewById(R.id.ivMinus)
	val ivPlus: ImageView = itemView.findViewById(R.id.ivPlus)
	val etQuantity: TextView = itemView.findViewById(R.id.etJumlah)
	val tilNote: EditText = itemView.findViewById(R.id.etNote)

	fun onBind(dataCart: Restoran, vmCart: CartViewModel) {
		// Isi elemen UI dengan data dari data_cart
		Glide.with(itemView.context)
			.load(dataCart.img)
			.into(ivGambar)

		tilNote.setText(dataCart.note)

		tvNama.text     = dataCart.name
		tvHarga.text    = dataCart.price.toString()
		etQuantity.text = dataCart.quantity.toString()

		ivDelete.setOnClickListener {
			vmCart.deleteItem(itemView.context,dataCart)
		}

		ivPlus.setOnClickListener{
			vmCart.increaseAmount(itemView.context,dataCart)
		}

		ivMinus.setOnClickListener{
			vmCart.decreaseAmount(itemView.context,dataCart)
		}

		tilNote.setOnEditorActionListener { _, actionId, event ->
			if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
				var note = tilNote.text.toString()
				vmCart.updateNote(itemView.context,dataCart,note)
			}
			false
		}
	}
}
