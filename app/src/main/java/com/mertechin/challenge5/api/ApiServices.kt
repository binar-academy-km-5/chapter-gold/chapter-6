package com.mertechin.challenge5.api

import com.mertechin.challenge5.dataclass.api_dataclass.OrderMenu
import com.mertechin.challenge5.dataclass.api_dataclass.OrderRes
import com.mertechin.challenge5.dataclass.api_dataclass.CategoryMenu
import com.mertechin.challenge5.dataclass.api_dataclass.ListMenu
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiServices {
	@GET("listmenu")
	fun getListMenu() : Call<ListMenu>

	@GET("category-menu")
	fun getCategory() : Call<CategoryMenu>

	@POST("order-menu")
	fun sendOrder(
		@Body orderMenu: OrderMenu
	): Call<OrderRes>

}