package com.mertechin.challenge5.dataclass.api_dataclass

data class ListMenu(
    val `data`: List<ListData>,
    val message: String,
    val status: Boolean
)