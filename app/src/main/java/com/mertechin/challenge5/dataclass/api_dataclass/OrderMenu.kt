package com.mertechin.challenge5.dataclass.api_dataclass

import com.google.gson.annotations.SerializedName

data class OrderMenu(
	var username        : String,
	var total           : Int,
	var orders           : List<OrderData>
)
