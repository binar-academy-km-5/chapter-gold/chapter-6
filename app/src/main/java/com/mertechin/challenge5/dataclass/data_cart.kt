package com.mertechin.challenge5.dataclass

data class data_cart(
	val img: Int,
	val name: String,
	val price: Int,
	val quantity: Int,
	val note: String
)
