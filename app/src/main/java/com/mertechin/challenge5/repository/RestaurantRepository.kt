package com.mertechin.challenge5.repository

import androidx.lifecycle.LiveData
import com.mertechin.challenge5.database.RestoranDAO
import com.mertechin.challenge5.dataclass.data_update
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran

class RestaurantRepository(private val database: RestoranDAO) {
	fun insertData(data: Restoran){
		database.insert(data)
	}

	fun measure() : LiveData<Int> {
		return database.measure()
	}

	fun getAllCart() : LiveData<List<Restoran>> {
		return database.getAllCart()
	}

	fun findItemCart(name : String) : List<Restoran>{
		return database.findItemCart(name)
	}

	fun findSomeDataByName(name: String) : data_update {
		return database.findSomeDataByName(name)
	}

	fun update(data : Restoran) {
		database.update(data)
	}

	fun updateQuantity(restoranId: Long, newQuantity: Int){
		database.updateQuantity(restoranId, newQuantity)
	}

	fun updateNote(cartId: Long, note: String){
		database.updateNote(cartId, note)
	}

	fun deleteAll(){
		database.deleteAll()
	}

	fun deleteWhere(id: Long){
		database.deleteWhere(id)
	}
}